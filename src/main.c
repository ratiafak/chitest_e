/**
 * @file main.c
 * @brief Main file of a math exam project to test whether digits of e are random enough.
 * @date 23 Nov 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "e_digits.h"

/**
 * @brief Output of Matlab's chi2inv(probabilities, df).
 * Element chi2_data[df][i] is chi-squared value for probability chi2_data[0][i],
 * where df is number of degrees of freedom.
 */
#define CHI2_DATA_SIZE      15
const double chi2_data[16][CHI2_DATA_SIZE] =
{
		//Probability
		{0.001000,0.002000,0.005000,0.010000,0.020000, 0.050000,0.250000,0.500000,0.750000,0.950000, 0.980000,0.990000,0.995000,0.998000,0.999000,},

		//Chi-squared values
		{0.000002,0.000006,0.000039,0.000157,0.000628, 0.003932,0.101531,0.454936,1.323304,3.841459, 5.411894,6.634897,7.879439,9.549536,10.827566,},
		{0.002001,0.004004,0.010025,0.020101,0.040405, 0.102587,0.575364,1.386294,2.772589,5.991465, 7.824046,9.210340,10.596635,12.429216,13.815511,},
		{0.024298,0.038681,0.071722,0.114832,0.184832, 0.351846,1.212533,2.365974,4.108345,7.814728, 9.837409,11.344867,12.838156,14.795517,16.266236,},
		{0.090804,0.129238,0.206989,0.297109,0.429398, 0.710723,1.922558,3.356694,5.385269,9.487729, 11.667843,13.276704,14.860259,16.923758,18.466827,},
		{0.210213,0.280140,0.411742,0.554298,0.751889, 1.145476,2.674603,4.351460,6.625680,11.070498, 13.388223,15.086272,16.749602,18.907377,20.515006,},
		{0.381067,0.486407,0.675727,0.872090,1.134419, 1.635383,3.454599,5.348121,7.840804,12.591587, 15.033208,16.811894,18.547584,20.791168,22.457744,},
		{0.598494,0.741057,0.989256,1.239042,1.564293, 2.167350,4.254852,6.345811,9.037148,14.067140, 16.622422,18.475307,20.277740,22.600671,24.321886,},
		{0.857105,1.037524,1.344413,1.646497,2.032477, 2.732637,5.070640,7.344121,10.218855,15.507313, 18.168231,20.090235,21.954955,24.352081,26.124482,},
		{1.151950,1.370205,1.734933,2.087901,2.532379, 3.325113,5.898826,8.342833,11.388751,16.918978, 19.679016,21.665994,23.589351,26.056433,27.877165,},
		{1.478743,1.734460,2.155856,2.558212,3.059051, 3.940299,6.737201,9.341818,12.548861,18.307038, 21.160768,23.209251,25.188180,27.721647,29.588298,},
		{1.833853,2.126459,2.603222,3.053484,3.608687, 4.574813,7.584143,10.340998,13.700693,19.675138, 22.617941,24.724970,26.756849,29.353638,31.264134,},
		{2.214209,2.543036,3.073824,3.570569,4.178287, 5.226029,8.438419,11.340322,14.845404,21.026070, 24.053957,26.216967,28.299519,30.956961,32.909490,},
		{2.617218,2.981550,3.565035,4.106915,4.765445, 5.891864,9.299066,12.339756,15.983906,22.362032, 25.471509,27.688250,29.819471,32.535215,34.528179,},
		{3.040673,3.439783,4.074675,4.660425,5.368197, 6.570631,10.165314,13.339274,17.116934,23.684791, 26.872765,29.141238,31.319350,34.091301,36.123274,},
		{3.482684,3.915852,4.600916,5.229349,5.984916, 7.260944,11.036538,14.338860,18.245086,24.995790, 28.259496,30.577914,32.801321,35.627600,37.697298,},
};

/**
 * @brief Multiply chain of digits and return what overflows the radix point.
 * @param digits array of digits
 * @param length number of digits
 * @return overflow integer part
 */
int multiply(uint8_t* digits, int multiple, int length)
{
	int i;
	int carry = 0;

	for(i = length; i >= 0; i--)
	{
		digits[i] *= multiple;	//Multiply
		digits[i] += carry;	//Add carry
		if(digits[i] >= 10)	//There is overflow
		{
			carry = digits[i] / 10;	//Carry over
			digits[i] %= 10;	//Keep only units
		}
		else	//No overflow
		{
			carry = 0;	//Nothing to carry over
		}
	}

	return carry;
}

/**
 * @brief Calculate chi^2 statistics V.
 * @note Presumes equal probability of all counts.
 * @param counts array of counts for each category
 * @param categories number of categories
 * @return V
 */
double calculate_v_stat(int* counts, int categories)
{
	int i;
	int total = 0;	//Number of observations of all categories
	int sum = 0;	//Running sum

	for(i = 0; i < categories; i++)
	{
		total += counts[i];
		sum += counts[i]*counts[i]*categories;	//Count^2/probability
	}

	return (double)sum/(double)total - (double)total;
}

/**
 * @brief Return threshold percentage from V stat.
 * @param df degrees of freedom (categories - 1)
 * @param v chi-squared statistics V
 * @return probability that V is over or under closest tabulated value [percent]
 */
double get_over_p_from_v(int df, double v)
{
	int i;
	for(i = 0; i < (CHI2_DATA_SIZE-1)/2; i++)
	{
		if(v < chi2_data[df][i]) return chi2_data[0][i];	//v is less than threshold
		if(v > chi2_data[df][CHI2_DATA_SIZE-1-i]) return chi2_data[0][CHI2_DATA_SIZE-1-i];	//v is more than threshold
	}
	return chi2_data[0][(CHI2_DATA_SIZE-1)/2];	//50
}

/**
 * @brief Printf test results.
 * @param base calculated for base
 * @param tested how many digits was tested
 * @param counts array of counts for each category
 */
void printf_results(int base, int tested, int* counts)
{
	double v, p;
	v = calculate_v_stat(counts, base);
	p = get_over_p_from_v(base-1, v);
	if((p >= 0.949) || (p <= 0.051))
	{
		printf("Converted %5.0i digits to base %2.0i with V = %6.3f, probability ~ %.3f  *** Hit! ***\n", tested, base, v, p);
	}
	else
	{
		printf("Converted %5.0i digits to base %2.0i with V = %6.3f, probability ~ %.3f\n", tested, base, v, p);
	}
}

/**
 * @brief Test digits of e in one base.
 * @param base what base to test
 * @param digits pointer to array of decimal digits
 * @param length number of digits
 * @return nonzero on error
 */
int test_base(int base, const uint8_t* digits, int length)
{
	int i;

	//Get memory for counts
	int* counts;
	if((counts = malloc(base*sizeof(int))) == NULL)
	{
		return 1;
	}
	memset(counts, 0x00, sizeof(int)*base);

	//Copy digits to new memory
	uint8_t* digits_mult;
	if((digits_mult = malloc(length)) == NULL)
	{
		free(counts);
		return 2;
	}
	memcpy(digits_mult, digits, length);

	//Multiply and get numbers
	for(i = 0; i < 2000; i++)
	{
		counts[multiply(digits_mult, base, length)]++;	//Increment count of the obtained digit
	}
	printf_results(base, i, counts);	//Calculate stats and print results

	for(; i < 10000; i++)
	{
		counts[multiply(digits_mult, base, length)]++;	//Increment count of the obtained digit
	}
	printf_results(base, i, counts);	//Calculate stats and print results

	free(digits_mult);
	free(counts);
	return 0;
}

/**
 * @brief Count digits and report current V in the process.
 * @param digits pointer to array of decimal digits
 * @param limit end on this many digits
 * @param resolution count this many and then print V
 * @return nonzero on error
 */
int graph_decimal(const uint8_t* digits, int limit, int resolution)
{
	int i;
	int counts[10];
	memset(counts, 0x00, sizeof(int)*10);

	printf("v_stats_v = [");
	for(i = 0; i < limit; i++)
	{
		counts[digits[i]]++;	//Increment count of the obtained digit
		if(i%resolution == resolution-1)
		{
			printf("%f ", calculate_v_stat(counts, 10));	//Print one V
		}
	}
	printf("];\n");
	printf("v_stats_n = %i:%i:%i;\n", resolution, resolution, i);

	return 0;
}

/**
 * @brief Main function of math exam project.
 * @return nonzero on error
 */
int main(void)
{
	int i;

	//Get length
	//int length = strlen(e_digits)-2;
	int length = 100000;	//Use only 100000 digits to speed it up

	//Pointer after the radix point
	uint8_t* digits = (uint8_t*)&e_digits[2];
	///@note Integer part is ignored.

	//Switch from string to decimal numbers
	for(i = 0; i < length; i++)
	{
		digits[i] -= '0';	//From '0' to 0x00
	}

	//Test each base
	for(i = 2; i < 16; i++)
	{
		test_base(i, digits, length);
	}

	printf("\nV stats for base 10:\n");
	graph_decimal(digits, 10000, 10);

	fflush(stdout);

	return 0;
}
